import { routes } from '~/configurations/site/routes';

export const Components = () => {
  let Components = {};

  routes.forEach((route) => {
    Components[route.alias] = require(`../components/${route.alias}`).default;
  });

  return Components;
};
