import React from 'react';
import store from '~/redux/store';
import { loadSite } from '~/redux/actions/core';
import TemplateLoaderComponent from '../TemplateLoader';
import { config } from '~/configurations/site/site';

function Site(receivedParams) {
  const loadSiteConfig = (siteName) => {
    console.log('Config', config);
    // Lookup the site configuration in redux.
    const site = store.getState().siteTemplates;

    // Verify we found the site configuration.
    if (site === undefined || site.name !== siteName) {
      // The site has not been loaded yet, import the configuration.
      store.dispatch(loadSite(config));
      // const promise = import(`../../configurations/site/site.json`).then(
      //   (result) => {
      //     console.log(result);
      //     // Store the site configuration in redux.
      //     store.dispatch(loadSite(result.config));
      //   }
      // );

      // throw promise;
    }
  };

  // Reference the route params.
  const {
    match: { params },
  } = receivedParams;

  // Retrieve the name of the site from the route, default if not found.
  const siteName = params.site === undefined ? 'default' : params.site;

  // Store the configuration in redux.
  loadSiteConfig(siteName);

  return <TemplateLoaderComponent {...receivedParams} />;
}

export default Site;
