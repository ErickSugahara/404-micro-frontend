import React from 'react';
import { useSelector } from 'react-redux';
import Zone from '../Zone';
import PropTypes from 'prop-types';
import MicroAppLoader from '../MicroAppLoader';

function TemplateLoader(receivedProps) {
  const siteTemplates = useSelector((store) => store.siteTemplates);
  const templateConfig = (alias, productLine, zone) => {
    // Attempt the retrieve the configuration by product line and id.
    let config = siteTemplates.templates.find((template) => {
      return template.productLine === productLine && template.alias === alias;
    });

    if (config === undefined) {
      // Attempt to retrieve the configuration by id.
      config = siteTemplates.templates.find((template) => {
        return template.productLine === undefined && template.alias === alias;
      });
    }

    if (zone !== null) {
      // Retrieve the configuration from the template zones.
      config = config.zones.find((obj) => {
        return obj.alias === zone;
      });
    }
    return config;
  };

  // Reference the route params.
  const {
    match: { params },
    zone,
  } = receivedProps;

  // Retrieve the name of the product line from the route.
  var productLineName = params.productLine;

  // Retrieve the name of the template from the route.
  var template = params.page === undefined ? 'default' : params.page;

  // Lookup the template configuration in redux.
  //
  // Zone is an optional value that may be present in props. Zone is used to
  // identify the location (zone) within a template that will recieve the hot loaded
  // component or micro-app. When loading the root page, zone will not be present.
  let config = templateConfig(template, productLineName, zone);

  let Placeholder;

  if (config.plugin.isMicroApp) {
    // Hot load the micro-app into the placeholder.
    Placeholder = <MicroAppLoader {...receivedProps} config={config} />;
  } else {
    // Hot load the component into the placeholder.
    Placeholder = <Zone {...receivedProps} config={config} />;
  }

  return Placeholder;
}

TemplateLoader.propTypes = {
  zone: PropTypes.string,
};

TemplateLoader.defaultProps = {
  zone: null,
};

export default TemplateLoader;
