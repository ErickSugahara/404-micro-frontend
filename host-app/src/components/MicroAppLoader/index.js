import React, { useEffect, useRef } from 'react';
import Script from 'react-load-script';

function MicroAppLoader(params) {
  const placeholder = useRef();

  useEffect(() => {
    if (window.loadedScripts !== undefined) {
      if (
        window[params.config.plugin.name] !== undefined &&
        window.loadedScripts.indexOf(params.config.plugin.path) > -1
      ) {
        // Render the micro app with props.
        window[params.config.plugin.name].mount(params, placeholder.current);
      }
    }
  }, [params]);

  const handleScriptCreate = () => {
    // console.log(`script ${params.config.plugin.path} created`);
  };

  const handleScriptError = () => {
    console.log(`script ${params.config.plugin.path} failed to load`);
  };

  const handleScriptLoad = () => {
    // Verify script collection has been instantiated.
    if (window.loadedScripts === undefined) {
      window.loadedScripts = [];
    }

    // Add the new script to the collection.
    window.loadedScripts.push(params.config.plugin);

    console.log(`script ${params.config.plugin.path} loaded`);

    if (window[params.config.plugin.name] !== undefined) {
      // Render the micro app with props.
      window[params.config.plugin.name].mount(params, placeholder.current);
    } else {
      throw Error(
        `Unable to mount micro-app ${params.config.plugin.name}, window.${params.config.plugin.name} is not defined`
      );
    }
  };

  let script;

  // Inject the bundle with a script tag.
  if (
    window.loadedScripts === undefined ||
    window.loadedScripts.indexOf(params.config.plugin.path) === -1
  ) {
    script = (
      <Script
        url={params.config.plugin.path}
        onCreate={handleScriptCreate}
        onError={handleScriptError}
        onLoad={handleScriptLoad}
      />
    );
  } else {
    script = null;
  }

  return (
    <div style={{ width: '100%' }}>
      {script}
      <div style={{ height: '100%' }} ref={placeholder}></div>
    </div>
  );
}

export default MicroAppLoader;
