import React from 'react';
import universal from 'react-universal-component';

const Zone = (params) => {
  let Component = null;

  const { config } = params;

  if (config != null && config.plugin != null) {
    Component = universal(import(`../../${config.plugin.path}`));

    return <Component {...params} {...config.props} config={config} />;
  }

  return null;
};

export default Zone;
