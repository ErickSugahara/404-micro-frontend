import React, { PureComponent } from 'react';
import TemplateLoaderComponent from '../TemplateLoader';

class MovieDetailsZone extends PureComponent {
  render() {
    return <TemplateLoaderComponent {...this.props} zone="movie-details" />;
  }
}

export default MovieDetailsZone;
