import React, { Suspense, useState } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import PrivateRoute from '~/components/PrivateRoute';
import SiteContainer from './components/Site';
import { routes } from '~/configurations/site/routes';
import { Components } from '~/utils/componetsIndex';
import './App.css';

function App() {
  const renderRoute = () => {
    const components = Components();
    return routes.map((route) => {
      const component = components[route.alias];
      return route.private ? (
        <PrivateRoute
          exact={route.exact}
          path={route.path}
          component={component}
        />
      ) : (
        <Route exact={route.exact} path={route.path} component={component} />
      );
    });
  };

  console.log('renderRoute()', renderRoute());

  return (
    <BrowserRouter basename="/">
      <div className="App">
        <Switch>
          {renderRoute()}
          <Route
            exact
            path="/:site/:productLine/:page/"
            component={SuspendedComponent()}
          />
          <Redirect from="*" to="/redbox/movies/search/" />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

function Teste() {
  return <div>teste</div>;
}

/**
 * Uses Suspense to async render the site component that is simulating
 * a rest call to retrieve the sites configuration from a backend system.
 */
function SuspendedComponent() {
  return (props) => (
    <Suspense fallback={<div>Loading...</div>}>
      <SiteContainer {...props} />
    </Suspense>
  );
}

export default App;
