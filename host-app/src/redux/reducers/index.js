import { combineReducers } from 'redux';

import { siteTemplates } from './core';

const Root = combineReducers({
  siteTemplates,
});

export default Root;
